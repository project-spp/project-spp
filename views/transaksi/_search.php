<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\TransaksiSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="transaksi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_transaksi') ?>

    <?= $form->field($model, 'id_petugas') ?>

    <?= $form->field($model, 'nisn') ?>

    <?= $form->field($model, 'jmlh_tagihan') ?>

    <?= $form->field($model, 'tgl_bayar') ?>

    <?php // echo $form->field($model, 'jmlh_byr') ?>

    <?php // echo $form->field($model, 'kekurangan') ?>

    <?php // echo $form->field($model, 'id_spp') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
