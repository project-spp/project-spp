<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Transaksi $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="transaksi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_transaksi')->textInput() ?>

    <?= $form->field($model, 'id_petugas')->textInput() ?>

    <?= $form->field($model, 'nisn')->textInput() ?>

    <?= $form->field($model, 'jmlh_tagihan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_bayar')->textInput() ?>

    <?= $form->field($model, 'jmlh_byr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kekurangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_spp')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'LUNAS' => 'LUNAS', 'BELUM LUNAS' => 'BELUM LUNAS', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
