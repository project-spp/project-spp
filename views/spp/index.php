<?php

use app\models\Spp;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\SppSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Spps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spp-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Spp', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_spp',
            'tahun',
            'nominal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Spp $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_spp' => $model->id_spp]);
                 }
            ],
        ],
    ]); ?>


</div>
