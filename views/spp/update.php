<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Spp $model */

$this->title = 'Update Spp: ' . $model->id_spp;
$this->params['breadcrumbs'][] = ['label' => 'Spps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_spp, 'url' => ['view', 'id_spp' => $model->id_spp]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="spp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
