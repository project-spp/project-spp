<?php

namespace app\controllers;

use app\models\Spp;
use app\models\SppSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SppController implements the CRUD actions for Spp model.
 */
class SppController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Spp models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SppSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Spp model.
     * @param int $id_spp Id Spp
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_spp)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_spp),
        ]);
    }

    /**
     * Creates a new Spp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Spp();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_spp' => $model->id_spp]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Spp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_spp Id Spp
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_spp)
    {
        $model = $this->findModel($id_spp);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_spp' => $model->id_spp]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Spp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_spp Id Spp
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_spp)
    {
        $this->findModel($id_spp)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Spp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_spp Id Spp
     * @return Spp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_spp)
    {
        if (($model = Spp::findOne(['id_spp' => $id_spp])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
