<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Spp;

/**
 * SppSearch represents the model behind the search form of `app\models\Spp`.
 */
class SppSearch extends Spp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_spp', 'tahun', 'nominal'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Spp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_spp' => $this->id_spp,
            'tahun' => $this->tahun,
            'nominal' => $this->nominal,
        ]);

        return $dataProvider;
    }
}
