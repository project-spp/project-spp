<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaksi".
 *
 * @property int $id_transaksi
 * @property int $id_petugas
 * @property int $nisn
 * @property string $jmlh_tagihan
 * @property string $tgl_bayar
 * @property string $jmlh_byr
 * @property string $kekurangan
 * @property int $id_spp
 * @property string $status
 */
class Transaksi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transaksi', 'id_petugas', 'nisn', 'jmlh_tagihan', 'tgl_bayar', 'jmlh_byr', 'kekurangan', 'id_spp', 'status'], 'required'],
            [['id_transaksi', 'id_petugas', 'nisn', 'id_spp'], 'integer'],
            [['tgl_bayar'], 'safe'],
            [['status'], 'string'],
            [['jmlh_tagihan', 'jmlh_byr', 'kekurangan'], 'string', 'max' => 55],
            [['id_transaksi'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_transaksi' => 'Id Transaksi',
            'id_petugas' => 'Id Petugas',
            'nisn' => 'Nisn',
            'jmlh_tagihan' => 'Jmlh Tagihan',
            'tgl_bayar' => 'Tgl Bayar',
            'jmlh_byr' => 'Jmlh Byr',
            'kekurangan' => 'Kekurangan',
            'id_spp' => 'Id Spp',
            'status' => 'Status',
        ];
    }
}
