<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transaksi;

/**
 * TransaksiSearch represents the model behind the search form of `app\models\Transaksi`.
 */
class TransaksiSearch extends Transaksi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transaksi', 'id_petugas', 'nisn', 'id_spp'], 'integer'],
            [['jmlh_tagihan', 'tgl_bayar', 'jmlh_byr', 'kekurangan', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaksi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_transaksi' => $this->id_transaksi,
            'id_petugas' => $this->id_petugas,
            'nisn' => $this->nisn,
            'tgl_bayar' => $this->tgl_bayar,
            'id_spp' => $this->id_spp,
        ]);

        $query->andFilterWhere(['like', 'jmlh_tagihan', $this->jmlh_tagihan])
            ->andFilterWhere(['like', 'jmlh_byr', $this->jmlh_byr])
            ->andFilterWhere(['like', 'kekurangan', $this->kekurangan])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
