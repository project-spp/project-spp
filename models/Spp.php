<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spp".
 *
 * @property int $id_spp
 * @property int $tahun
 * @property int $nominal
 */
class Spp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_spp', 'tahun', 'nominal'], 'required'],
            [['id_spp', 'tahun', 'nominal'], 'integer'],
            [['id_spp'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_spp' => 'Id Spp',
            'tahun' => 'Tahun',
            'nominal' => 'Nominal',
        ];
    }
}
