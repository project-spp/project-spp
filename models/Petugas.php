<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "petugas".
 *
 * @property int $id_petugas
 * @property int $username
 * @property int $password
 * @property int $nama
 * @property string $level
 */
class Petugas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'petugas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_petugas', 'username', 'password', 'nama', 'level'], 'required'],
            [['id_petugas', 'username', 'password', 'nama'], 'integer'],
            [['level'], 'string'],
            [['id_petugas'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_petugas' => 'Id Petugas',
            'username' => 'Username',
            'password' => 'Password',
            'nama' => 'Nama',
            'level' => 'Level',
        ];
    }
}
