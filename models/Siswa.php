<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "siswa".
 *
 * @property int $nisn
 * @property string $nis
 * @property string $nama
 * @property int $id_kelas
 * @property string $alamat
 * @property string $tlp
 * @property int $id_spp
 * @property string $jmlh_tagihan
 */
class Siswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'siswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nisn', 'nis', 'nama', 'id_kelas', 'alamat', 'tlp', 'id_spp', 'jmlh_tagihan'], 'required'],
            [['nisn', 'id_kelas', 'id_spp'], 'integer'],
            [['alamat'], 'string'],
            [['nis'], 'string', 'max' => 8],
            [['nama'], 'string', 'max' => 255],
            [['tlp'], 'string', 'max' => 15],
            [['jmlh_tagihan'], 'string', 'max' => 55],
            [['nisn'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nisn' => 'Nisn',
            'nis' => 'Nis',
            'nama' => 'Nama',
            'id_kelas' => 'Id Kelas',
            'alamat' => 'Alamat',
            'tlp' => 'Tlp',
            'id_spp' => 'Id Spp',
            'jmlh_tagihan' => 'Jmlh Tagihan',
        ];
    }
}
