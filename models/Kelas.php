<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kelas".
 *
 * @property int $id_kelas
 * @property string $nama_kelas
 * @property string $kompetensi_keahlian
 */
class Kelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kelas', 'nama_kelas', 'kompetensi_keahlian'], 'required'],
            [['id_kelas'], 'integer'],
            [['nama_kelas'], 'string', 'max' => 10],
            [['kompetensi_keahlian'], 'string', 'max' => 50],
            [['id_kelas'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kelas' => 'Id Kelas',
            'nama_kelas' => 'Nama Kelas',
            'kompetensi_keahlian' => 'Kompetensi Keahlian',
        ];
    }
}
